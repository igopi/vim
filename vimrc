" theme {{{
colorscheme badwolf
syntax enable
" }}}
" indent {{{
set tabstop=4
set softtabstop=4
set expandtab
set autoindent
set copyindent
set shiftwidth=4
set shiftround
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.
" }}}
" UI {{{
set number
set relativenumber
set cursorline
set ruler
filetype indent on
set wildmenu
set wildignore=*~,*.pyc
set wildignore+=*/.git/*
set lazyredraw
set showmatch
set modeline
set pastetoggle=<F2>
set so=5
set laststatus=2
" }}}
" Window {{{
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
" }}}
" Search {{{
set incsearch
set hlsearch
set ignorecase
set smartcase
set path +=**
nnoremap <leader><space> :nohlsearch<CR>
" }}}
" Folding {{{
set foldmethod=indent
set foldnestmax=10
set foldenable
set foldlevelstart=1
nnoremap <space> za
" }}}
" Move {{{
nnoremap j gj
nnoremap k gk
nnoremap <Left>  :echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up>    :echoe "Use k"<CR>
nnoremap <Down>  :echoe "Use j"<CR>
" }}}
" Leader {{{
nnoremap <leader>s :mksession<CR>
" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>
" }}}
" History {{{
set history=500
"}}}
" Backup {{{
set nobackup
set nowb
set noswapfile
"}}}
" Delete trailing space {{{
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.py,*.sh :call CleanExtraSpaces()
endif
" }}}
" Rac {{{
nnoremap <leader>r :!%:p
"}}}
" vim:foldmethod=marker:foldlevel=0
